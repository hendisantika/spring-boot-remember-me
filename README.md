# Spring Boot Remember Me

Run this project by these commands :

1. `git clone https://gitlab.com/hendisantika/spring-boot-remember-me.git`
2. `cd spring-boot-remember-me`
3. `mvn clean spring-boot:run`
4. Use this username & password to login (naruto / 123456)

### Screen shot

Login Page

![Login Page](img/home.png "Login Page")

Admin Page

![Admin Page](img/admin.png "Admin Page")

Session Id

![Session Id](img/session.png "Session Id")